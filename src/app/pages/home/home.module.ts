import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ProfilePageModule } from '../profile/profile.module';
import { MunicipalityListPageModule } from '../municipality-list/municipality-list.module';
import { SettingsPageModule } from '../settings/settings.module';
import { PostsPageModule } from '../posts/posts.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    FontAwesomeModule,
    ProfilePageModule,
    MunicipalityListPageModule,
    SettingsPageModule,
    PostsPageModule,
  ],
  declarations: [HomePage],
})
export class HomePageModule {}
