import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import L, { icon, LatLngExpression, Map, Marker, marker, tileLayer } from 'leaflet';
import { takeUntil } from 'rxjs/operators';
import { ReportsService } from '../../services/reports.service';
import { Subject } from 'rxjs';
import { animate, style, transition, trigger } from '@angular/animations';
import { AuthService } from '../../auth/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { FullscreenImagePage } from '../../modals/fullscreen-image/fullscreen-image.page';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.page.html',
  styleUrls: ['./post-details.page.scss'],
  animations: [
    trigger('fade', [transition('void => *', [style({ opacity: 0 }), animate(1000, style({ opacity: 1 }))])]),
  ],
})
export class PostDetailsPage implements OnInit {
  @ViewChild('map', { read: ElementRef, static: true }) mapElement: ElementRef;

  detailPost: any[] = [];
  private geocode: LatLngExpression = [0, 0];
  style: any = { height: '0%' };
  private marker: Marker<any>;
  hasAddress = false;
  private unsubscribe$ = new Subject<void>();
  comments: any[] = [];
  myId: string;
  postsForm: FormGroup;
  isSubmitted: boolean = false;
  imageLoading: boolean = true;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private serviceReport: ReportsService,
    private authService: AuthService,
    public formBuilder: FormBuilder,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public modalController: ModalController
  ) {
    this.route.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.detailPost.push(this.router.getCurrentNavigation().extras.state.detailPost);
      }
    });

    this.postsForm = this.formBuilder.group({
      text: ['', [Validators.required]],
    });
  }

  ngOnInit() {}

  ionViewDidEnter() {
    if (this.detailPost[0].address && this.detailPost[0].address.hasOwnProperty('address')) {
      this.geocode = [
        parseFloat(this.detailPost[0].address.latitude),
        parseFloat(this.detailPost[0].address.longitude),
      ];
      this.style = { height: '33%' };
      this.hasAddress = true;
      setTimeout(() => {
        this.initMap();
      }, 1000);
    } else {
      this.style = { height: '0' };
    }

    this.getCurrentUser();
  }

  initMap() {
    const map = new Map(this.mapElement.nativeElement).setView(this.geocode, 18);
    tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(map);

    const customMarkerIcon = icon({
      iconUrl: 'assets/icon/marker.png',
      iconSize: [40, 102],
      popupAnchor: [0, -40],
    });

    this.marker = marker(this.geocode, { icon: customMarkerIcon })
      .bindPopup(`<b>${this.detailPost[0].address.address}</b>`, { autoClose: false })
      .addTo(map)
      .openPopup();

    this.centerLeafletMapOnMarker(map, this.marker);
  }

  centerLeafletMapOnMarker(map, marker) {
    const latLngs = [marker.getLatLng()];
    const markerBounds = L.latLngBounds(latLngs);
    map.fitBounds(markerBounds);
  }

  getStatusPost(status: string) {
    if (status === 'pending') {
      return 'Pendente';
    } else if (status === 'open') {
      return 'Aperta';
    } else if (status === 'close') {
      return 'Chiusa';
    }
  }

  getComments() {
    this.serviceReport
      .getReportById(this.detailPost[0].id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (res) => {
          this.comments = res.items;
        },
        (error) => {},
        () => {
          // this.isLoading = false;
        }
      );
  }

  getCurrentUser() {
    this.authService
      .getCurrentUser()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (res) => {
          this.myId = res.id;
          this.getComments();
        },
        (error) => {},
        () => {
          // this.isLoading = false;
        }
      );
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  get errorControl() {
    return this.postsForm.controls;
  }

  async onSubmit() {
    this.isSubmitted = true;
    if (!this.postsForm.valid) {
      console.log('Please provide all the required values!');
      return false;
    } else {
      const loading = await this.loadingController.create({
        cssClass: 'my-custom-class',
        message: 'Invio commento in corso...',
      });
      await loading.present().then(() => {
        this.serviceReport
          .createCommentPost(this.postsForm.value, this.detailPost[0].id)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(
            (res) => {
              this.presentAlertPost('Commento inviato con successo', '');
              this.postsForm.reset();
              this.postsForm.clearValidators();
              this.getComments();
            },
            (error) => {
              this.presentAlertPost('Opss!', 'Ci sono stati dei problemi, riprova più tardi!');
            },
            () => {
              loading.dismiss();
            }
          );
      });
    }
  }

  async presentAlertPost(header: string, message: string) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: ['Chiudi'],
    });

    await alert.present();
  }

  imageLoaded() {
    this.imageLoading = false;
  }

  async showFullScreenImage(imageUrl: string) {
    const modal = await this.modalController.create({
      component: FullscreenImagePage,
      cssClass: 'my-custom-class',
      componentProps:{
        'imageUrl': imageUrl
      }
    });
    return await modal.present();
  }

}
