import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewPostPageRoutingModule } from './new-post-elements-routing.module';

import { NewPostElementsPage } from './new-post-elements.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, NewPostPageRoutingModule, ReactiveFormsModule],
  declarations: [NewPostElementsPage],
})
export class NewPostElementsPageModule {}
